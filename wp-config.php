<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'demoproject' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'homestead' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'secret' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Un(@3A:A4;yHg}#m446-V{ y(Tpa<}5fh-e6zdx]Hm<kja$=,Cu}p2t+yFQL,}S7' );
define( 'SECURE_AUTH_KEY',  ';8H,YN-XzQ.EPXa8gSX^{faFIM5`gRfl5FSJomS%8_p2Rg)DUU~qz62r>Wt+|%+?' );
define( 'LOGGED_IN_KEY',    'KaDNDl!lFhr7tlf)+io|Xz-OYvlxVE+:PA*]EHeJm?=Lk,!@nbCKr8D9M))i6v~$' );
define( 'NONCE_KEY',        'pmlj5)<|]Nsj3VG.y[KNQ2hjpN; XakM1G]e&z<b~goA~2wGWhX8o(q/(,u~z<Fy' );
define( 'AUTH_SALT',        'FsJ4ojkgY!0.[&3$vH;>i!QvWFh;bcjCC`pJ.%),No7#}7L8oQ:`&?USn.16B<IE' );
define( 'SECURE_AUTH_SALT', 'do[/*Z?j+;[F),IKpuXV<Gz69fTTdQIIDMmiFda^%9/SwwU6et;3N~uJ:[[8hX-<' );
define( 'LOGGED_IN_SALT',   '+u?$4Q#b`28*y@Zw49,!$H5!!MEn$p I!yZv@9L%}=V9u/-+8c9=Sq7M1LM@Tq|V' );
define( 'NONCE_SALT',       'rS5d,q{2/?@[,hhAdY1`my>61C(.NUH;SL~IbmMJgO(#K&zYi&pUbG`V`yu?2@U[' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
