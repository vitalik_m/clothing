<?php

 $url_urich = get_url_for_file('img'); 
 $mark = get_array_bookmarks();
 //cl_print_r($mark);
 $product333 = $GLOBALS['product'];
if (have_posts()): while (have_posts()) : the_post();

            $product_id = get_the_ID();
            $product = wc_get_product($product_id);
            $link = get_the_permalink($product_id);
            $url_urich = get_url_for_file('img'); 
            $product_name = $product->get_name();
             $symbol = get_woocommerce_currency_symbol();
            $price = $product->get_regular_price() . ' ' .$symbol;
            if (has_post_thumbnail()) $product_image = get_the_post_thumbnail_url($product_id, 'medium');
            $color = $product->get_attribute('pa_color');
            $color = explode(', ', $color);
            $rate = intval(get_field('rate'));
           
            //cl_print_r($symbol);
            ?>
          <a href="<?php echo $link; ?>" class="a-selected-item-line-wrapper">
            <div class="selected-item-line-wrapper">
              <div class="selected-item-line-img-wrapper">
                <img class="selected-item-line-img" src="<?php echo $product_image; ?>" alt="item-image" />
              </div>
              <div class="selected-item-line-info-wrapper">
                <p class="selected-item-line-info-title"><?php echo $product_name; ?></p>

                <div class="selected-item-line-info-rate">
                <?php for($i = 1; $i < 6; $i++){ 

                    if($i <= $rate){ ?>
                       <i class="fa fa-star gold" aria-hidden="true"></i>
                    <?php } else { ?>
                          <i class="fa fa-star-o bronze" aria-hidden="true"></i>
                <?php } 
                  }?>
                </div>

                <div class="selected-item-line-info-color-wrapper">
                  <?php 
                    if($color ){
                       foreach($color as $key=>$val){
                         if($val){
                           if($key<10)
                           echo '<div class="color" style="background: '.$val.' "></div>';
                         }
                      }
                    }
                  ?>
                </div>
                <p class="tab-selected-item-p">from <strong><?php echo $price; ?></strong></p>
                <?php if(array_search($product_id, $mark)) { ?>
                <div class="bookmark-ico-container bookmark-active" id="<?php echo $product_id; ?>">
                  <?php } else { ?>
                  <div class="bookmark-ico-container" id="<?php echo $product_id; ?>">
                <?php } ?>
                <img class="bookmark-ico" src="<?php echo $url_urich; ?>assets/bookmarks.svg" alt="" />
              </div>
              </div>
            </div>
          </a>
                
<?php endwhile;?>
<?php endif; ?>


