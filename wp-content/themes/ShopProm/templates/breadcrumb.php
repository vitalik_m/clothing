<?php
/*
 * Breadcrumb template loop
 */
?>

<div class="pagination-destination">
    <a href="<?php echo get_home_url(); ?>" class="pagination-destination-road">Main</a>
    <p class="pagination-destination-road">-</p>
        <?php if (is_404()): ?>
        <a href="<?php echo get_home_url(); ?>" class="pagination-destination-road">Main</a>
        <p class="pagination-destination-road">-</p>
        <p class="pagination-destination-road">404</p>

        <?php elseif (is_search()): ?>
        <a href="<?php echo get_home_url(); ?>/category/" class="pagination-destination-road">Category</a>
            <p class="pagination-destination-road" style="margin-left: 5px; margin-right: 5px;">-</p>
        <p class="pagination-destination-road">Search Result</p>

        <?php elseif (is_account_page() || is_page_template('page-shop.php')): ?>
             <a href="<?php echo get_home_url(); ?>/category/" class="pagination-destination-road">Category</a>

      
        <?php elseif (is_single()): ?>
            <a href="<?php echo get_home_url(); ?>/category/" class="pagination-destination-road">Category</a>
            <p class="pagination-destination-road" style="margin-left: 5px; margin-right: 5px;">-</p>
            <p class="pagination-destination-road"><?php the_title(); ?></p>

        <?php else: ?>
        <a href="<?php echo get_home_url(); ?>/category/" class="pagination-destination-road">Category</a>

        <?php endif; ?>
</div>


            
      