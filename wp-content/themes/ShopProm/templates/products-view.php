<?php
 $url_urich = get_url_for_file('img'); 
 $mark = get_array_bookmarks();
if (have_posts()): while (have_posts()) : the_post();
            $product_id = get_the_ID();
            $product = wc_get_product($product_id );
            $link = get_permalink($product_id );
            $url_urich = get_url_for_file('img'); 
            $product_name = $product->get_name();
            $symbol = get_woocommerce_currency_symbol();
            $price = $product->get_regular_price() . ' ' .$symbol;
            $tags = get_the_terms(get_the_ID(), 'product_tag');
            (is_object($tags[0])) ? $tag = $tags[0]->name : $tag = null;
            if (has_post_thumbnail()) $product_image = get_the_post_thumbnail_url($product_id , 'medium');
            ?>
                <a href="<?php echo $link; ?>" class="a-tab-selected-item"
                  ><div class="tab-selected-item">
                    <?php if($tags) {
                       echo '<div class="discounted-item-25" style="background: #e30613; color: #ffffff">'.$tag.'</div>';
                     } ?>
                    <img class="tab-selected-item-img" src="<?php echo $product_image; ?>" alt="item-image" />
                    <p class="tab-selected-item-p"><?php echo $product_name; ?></p>
                    <p class="tab-selected-item-price
                    ">from <strong><?php echo $price; ?></strong></p>
                    <?php if(array_search($product_id, $mark)) { ?>
                <div class="bookmark-ico-container bookmark-active" id="<?php echo $product_id; ?>">
                  <?php } else { ?>
                  <div class="bookmark-ico-container" id="<?php echo $product_id; ?>">
                <?php } ?>
                <img class="bookmark-ico" src="<?php echo $url_urich; ?>assets/bookmarks.svg" alt="" />
              </div>
                  </div></a>
                    <?php endwhile;?>
<?php endif; 


