
<?php $url_urich = get_url_for_file('img'); ?>
<section class="section-about">
<div class="about wrapper">
  <h2 class="title-h2">about us</h2>
<?php
  $posts = get_posts( array(
		'category'    => 41,
    'post_type'   => 'post',
    'orderby' => 'post_modified'
	) ); 
	$i=0;
	foreach( $posts as $post ){
		if($post && $i<2){
				$text = get_the_content();
			 preg_match("|<p>(.*)</p>|is", $post->post_content, $result); ?>
		<?php //cl_print_r($result); ?>
    <div class="about_name">

    <div class="about_name_img_wrapper">
      <?php echo get_the_post_thumbnail($post->ID, array(370, 248)); ?>    
    </div>

    <div class="about_name_p_wrapper">
      <p class="about_name_p"><?php echo $result[1]; ?></p>
    </div>
  </div>
	   <?php
	   $i++;
		}
	} wp_reset_postdata(); // сброс 
?>

  <a href="<?php echo get_home_url(). '/service/'; ?>" class="add-btn">Read more</a>
</div>

</section>


