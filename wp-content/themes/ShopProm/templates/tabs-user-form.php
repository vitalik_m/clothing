<?php

$user_id = get_current_user_id();
$user = get_userdata($user_id);
$user_meta = get_user_meta($user_id);
global $wpdb;
$res_location = $wpdb->get_results("SELECT `meta_value` FROM `wp_usermeta` WHERE `user_id`='$user_id' AND `meta_key`='_location_user'");
$res_phone = $wpdb->get_results("SELECT `meta_value` FROM `wp_usermeta` WHERE `user_id`='$user_id' AND `meta_key`='_mobile_phone'");
$res_name = $wpdb->get_results("SELECT `meta_value` FROM `wp_usermeta` WHERE `user_id`='$user_id' AND `meta_key`='first_name'");
$res_last_name = $wpdb->get_results("SELECT `meta_value` FROM `wp_usermeta` WHERE `user_id`='$user_id' AND `meta_key`='last_name'");
 foreach($res_location as $value){
  $user_location = $value->meta_value;
   }
   foreach($res_phone as $value){
    $user_phone = $value->meta_value;
     }
     foreach($res_name as $value){
      $user_name = $value->meta_value;
       }
       foreach($res_last_name as $value){
        $user_last_name = $value->meta_value;
         }
?>




 <form action="" method="post" class="tab-dflex" id="account_info_changes" onsubmit="javascript:return validate('account_info_changes','email_check');">
  <p class="person-tab-advice">You can edit the information that is displayed on your profile.</p>
  
  <div class="single-fields-container">
 
    <div class="single-field-wrapper">
      <p class="single-field-p">First Name</p>
      <input class="single-field-input" name="user_name" maxlength="15" type="text" placeholder="<?php echo $user_name; ?>" />
    </div>

    <div class="single-field-wrapper">
      <p class="single-field-p">Location</p>
      <input class="single-field-input" name="user_location" maxlength="50" type="text" placeholder="<?php echo $user_location; ?>" />
    </div>

    <div class="single-field-wrapper">
      <p class="single-field-p">Last Name</p>
      <input class="single-field-input" name="user_last_name" maxlength="15" type="text" placeholder="<?php echo $user_last_name; ?>" />
    </div>

    <div class="single-field-wrapper">
      <p class="single-field-p">Phone Number</p>
      <input class="single-field-input" id="cell_phone" name="user_phone" type="tel" maxlength="15" placeholder="<?php echo $user_phone; ?>" />
    </div>

    <div class="single-field-wrapper">
      <p class="single-field-p">Email</p>
      <input class="single-field-input" name="user_email" type="email" placeholder="<?php echo $user->data->user_email; ?>" />
    </div>

    <div class="single-field-wrapper">
      <p class="single-field-p">Password</p>
      <input class="single-field-input" name="password" type="password" placeholder="Password" />
    </div>

  </div>
  <button class="contact-form-btn">Save changes</button>
  </form>





