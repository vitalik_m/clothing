﻿<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

if (isset($_GET['select']) && $_GET['select'] == 'top+sales') { $order = "&orderby=date&order=DESC"; $s1 = ' selected="selected"'; }
if (isset($_GET['select']) && $_GET['select'] == 'top') { $order = "&orderby=date&order=ASC"; $s2 = ' selected="selected"'; }
if (isset($_GET['select']) && $_GET['select'] == 'options') { $order = "&orderby=options&order=ASC"; $s3 = ' selected="selected"'; }

$modeType = 'block';
		if(isset($_COOKIE['prod-type-view']) && $_COOKIE['prod-type-view'] == 'list'){
				$modeType = 'list';
		}  

get_header();
?>
   <section class="wrapper-filter">
        <div class="filter wrapper">
          <?php  sub_menu_filter(); ?>  
          <?php //echo sub_menu_other_filter(); ?>
        </div> 
        <div id="js_filter_more" class="filter wrapper" style="display: none">
          <?php  sub_menu_filter(); ?>  
        </div>
	  </section>
	  
	  <section class="wrapper-pagination">
        <div class="pagination wrapper">
          <?php echo do_action('breadcrumb'); ?>
          <div class="pagination-stuff">
            <div class="pagination-margin pagination-stuff-sortby">
              <p class="pagination-stuff-sortby-text">sort by:</p>
              <select class="sort-select" name="" id="sort"
                ><option selected value="top rated">top rated</option
                ><option value="top sales">top sales</option
                ><option value="top">top</option
                ><option value="options">options</option></select
              >
            </div>
            <div class="pagination-margin pagination-stuff-page-number">
			<?php //echo get_pagination_page(); ?> 
			</div>

          <?php 
          ?>
            <div class="pagination-margin pagination-stuff-list">
              <div class="list-swap-wrapper active <?php if($modeType != 'block')  echo 'cust-no-active'?>" id="block" onclick="filterMode(this,  'block')">
                <i class="fa fa-th" aria-hidden="true"></i></div>
              <div class="list-swap-wrapper active <?php if($modeType == 'block')  echo 'cust-no-active'  ?>" id="list"  onclick="filterMode(this,  'list')">
                <i class="fa fa-list-ul" aria-hidden="true"></i></div>
            </div>

          </div>
        </div>
      </section>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">
            <?php $search = new WP_Query("s=$s&showposts=-1");  ?>
            <?php if ( have_posts() ) : ?>

            <div class="plate-result-destination">
              <p class="plate-result-destination-text" id="get_search_count"><?php echo $search->post_count; ?></p>
              <p class="plate-result-destination-text">searches for</p>
              <p class="plate-result-destination-text"><?php echo get_search_query(); ?></p>
            </div>
            <!-- <header class="page-header">
                <h1 class="page-title">
                    <?php _e( 'searches for:', 'twentynineteen' ); ?>
                </h1>
                <div class="page-description"><?php echo get_search_query(); ?></div>
            </header>.page-header -->

            <section class="wrapper-plate">
                <div class="plate wrapper">
                    <?php
                    global $query_string; // параметры базового запроса
                    query_posts($query_string.'&'.$order); // базовый запрос + свои параметры

                    if($modeType == 'block'){
                            get_template_part('templates/products-view');
                                echo '<a href="#" class="flex-empty"></a>
                                            <a href="#" class="flex-empty"></a>
                                            <a href="#" class="flex-empty"></a>';
                    }else{
                        get_template_part('templates/products-view-line');
                            echo '<a href="#" class="flex-empty-line"></a>
                                        <a href="#" class="flex-empty-line"></a>
                                        <a href="#" class="flex-empty-line"></a>';
                        }
                    else :
                        get_template_part( 'template-parts/content/content', 'none' );
                    endif;
                    ?>
                </div>
                <div class="showmore"><div id="showmore-button" class="showmore-btn" style="display: none">show more</div></div>
            </section>
		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();

// function get_pagination_page(){
//   return twentynineteen_the_posts_navigation();
// }

?>