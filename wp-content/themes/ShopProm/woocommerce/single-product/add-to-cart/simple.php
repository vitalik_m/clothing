<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
$symbol = get_woocommerce_currency_symbol();
if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.
//  cl_print_r($product->price);
if ( $product->is_in_stock() ) : ?>
<!-- 11111111111111111111111111111 -->
	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
	<!-- 22222222222222222222222 -->
	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>"
          method="post" enctype='multipart/form-data'>

	    <?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

        <!-- 3333333333333333 -->
		<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );

		woocommerce_quantity_input( array(
			'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
			'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
		) );

		do_action( 'woocommerce_after_add_to_cart_quantity' );
		?>
        <!-- 444444444444 -->
		<button type="submit"
		name="add-to-cart"
		value="<?php echo esc_attr( $product->get_id() ); ?>"
		class="button alt add-btn"
		><?php echo esc_html( $product->single_add_to_cart_text() );?></button>

		<!-- <button type="submit"
		name="add-to-cart"
		value="<?php echo esc_attr( $product->get_id() ); ?>"
		class="single_add_to_cart_button button alt add-btn"
		><?php echo esc_html( $product->single_add_to_cart_text() );?></button> -->
		<div class="description-product-table">
            <table class="tierprice-table" style="height: 346px;" width="755">
                <thead>
                    <tr class="tierprice-table__trSecond">
                        <th>
                            <pre id="tw-target-text" class="tw-data-text tw-ta tw-text-large" dir="ltr" data-placeholder="Перевод"><span lang="en" tabindex="0">From piece</span></pre>
                        </th>
                        <!-- <th><strong>1</strong></th> -->
                    </tr>
                </thead>
                <tbody>
                    <tr class="odd">
                        <td><strong>$ / Stk.</strong></td>
                        <!-- <td><?php echo $product->price.' '.$symbol; ?> </td> -->
                        <td class="printprices spezial_sieb_druck-2" style="display: none;">–</td>
                        <td class="printprices spezial_sieb_druck-2" style="display: none;">–</td>
                        <td class="printprices spezial_sieb_druck-2" style="display: none;">–</td>
                        <td class="printprices spezial_sieb_druck-2" style="display: none;">–</td>
                        <td class="printprices spezial_sieb_druck-3" style="display: none;">–</td>
                        <td class="printprices spezial_sieb_druck-3" style="display: none;">–</td>
                        <td class="printprices spezial_sieb_druck-3" style="display: none;">–</td>
                        <td class="printprices spezial_sieb_druck-3" style="display: none;">–</td>
                        <td class="printprices stickerei-4" style="display: none;">–</td>
                        <td class="printprices stickerei-4" style="display: none;">–</td>
                        <td class="printprices stickerei-4" style="display: none;">–</td>
                        <td class="printprices stickerei-4" style="display: none;">–</td>
                    </tr>
                    <tr class="odd">
                    <td><strong>$ / Stk. Design</strong></td>
                    <!-- <td><?php echo $product->price.' '.$symbol; ?> </td> -->
                    <td class="printprices spezial_sieb_druck-2" style="display: none;">–</td>
                    <td class="printprices spezial_sieb_druck-2" style="display: none;">–</td>
                    <td class="printprices spezial_sieb_druck-2" style="display: none;">–</td>
                    <td class="printprices spezial_sieb_druck-2" style="display: none;">–</td>
                    <td class="printprices spezial_sieb_druck-3" style="display: none;">–</td>
                    <td class="printprices spezial_sieb_druck-3" style="display: none;">–</td>
                    <td class="printprices spezial_sieb_druck-3" style="display: none;">–</td>
                    <td class="printprices spezial_sieb_druck-3" style="display: none;">–</td>
                    <td class="printprices stickerei-4" style="display: none;">–</td>
                    <td class="printprices stickerei-4" style="display: none;">–</td>
                    <td class="printprices stickerei-4" style="display: none;">–</td>
                    <td class="printprices stickerei-4" style="display: none;">–</td>
                </tr>
                </tbody>
            </table>
        </div>
		<?php $value_design_arr = get_new_price_design_func($product->get_id());
			$value_design = array_chunk($value_design_arr, 3);
			//cl_print_r(count($value_design_arr));
		?>
		<pre style="height: 15px;"></pre>
		<?php if(count($value_design_arr) != 0) { ?>
<!--		<table class="tierprice-table-design" -->
<!--		 width="755">-->
<!--			<thead>-->
<!--				<tr class="tierprice-table__trSecond-design">-->
<!--				<th>-->
<!--				<pre id="tw-target-text-design" class="tw-data-text tw-ta tw-text-large-design" dir="ltr" data-placeholder="Перевод"><span lang="en" tabindex="0">From piece design</span></pre>-->
<!--				</th>-->
<!--				--><?php //for($i = 0; $i < count($value_design); $i++){
//				$k = 0;	?>
<!--					--><?php //foreach($value_design[$i] as $value) { ?><!--	-->
<!--							--><?php //if($k == 0) { ?>
<!--								<th>--><?php //echo $value; ?><!--</th>-->
<!--							--><?php	//} ?>
<!--					--><?php //$k++;	} ?>
<!--				--><?php	//} ?>
<!--				<!-- <th><strong>1</strong></th> -->
<!--				</tr>-->
<!--			</thead>-->
<!--			<tbody>-->
<!--			<tr class="odd-design">-->
<!--			<td><strong>$ / Stk.</strong></td>-->
<!---->
<!--			--><?php //for($i = 0; $i < count($value_design); $i++){
//				$k = 0;	?>
<!--					--><?php //foreach($value_design[$i] as $value) { ?><!--	-->
<!--							--><?php //if($k == 2) { ?>
<!--								<td>--><?php //echo $value.' '.$symbol;; ?><!--</td>-->
<!--							--><?php	//} ?>
<!--					--><?php //$k++;	} ?>
<!--				--><?php	//} ?>
<!---->
<!--			<!-- <td>--><?php //echo $product->price.' '.$symbol; ?><!-- </td> -->
<!--			<td class="printprices spezial_sieb_druck-2" style="display: none;">–</td>-->
<!--			<td class="printprices spezial_sieb_druck-2" style="display: none;">–</td>-->
<!--			<td class="printprices spezial_sieb_druck-2" style="display: none;">–</td>-->
<!--			<td class="printprices spezial_sieb_druck-2" style="display: none;">–</td>-->
<!--			<td class="printprices spezial_sieb_druck-3" style="display: none;">–</td>-->
<!--			<td class="printprices spezial_sieb_druck-3" style="display: none;">–</td>-->
<!--			<td class="printprices spezial_sieb_druck-3" style="display: none;">–</td>-->
<!--			<td class="printprices spezial_sieb_druck-3" style="display: none;">–</td>-->
<!--			<td class="printprices stickerei-4" style="display: none;">–</td>-->
<!--			<td class="printprices stickerei-4" style="display: none;">–</td>-->
<!--			<td class="printprices stickerei-4" style="display: none;">–</td>-->
<!--			<td class="printprices stickerei-4" style="display: none;">–</td>-->
<!--			</tr>-->
<!--			</tbody>-->
<!--		</table>-->
		<?php	} ?>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>
<!-- 555555555555 -->
	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
