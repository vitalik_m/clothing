<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.2
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_lost_password_form' );
?>
<section class="no-results not-found error-wrapper">
<div class="wrapper error">
<form method="post" class="woocommerce-ResetPassword lost_reset_password">

	<p>Lost your password? Please enter your username or email address. 
	You will receive a link to create a new password via email.</p>

	<p style="width: 100%;" class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label style="margin: 0 auto;" for="user_login">Username or email</label>
		<input class="woocommerce-Input woocommerce-Input--text input-text" type="text" name="user_login" id="user_login" autocomplete="username" />
	</p>

	<div class="clear"></div>

	<?php do_action( 'woocommerce_lostpassword_form' ); ?>

	<p class="woocommerce-form-row form-row">
		<input type="hidden" name="wc_reset_password" value="true" />
		<button style="color: #ffffff; margin: 0 auto;" type="submit" class="woocommerce-Button button add-btn" value="<?php esc_attr_e( 'Reset password', 'woocommerce' ); ?>">Reset password</button>
	</p>

	<?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>

</form>
</div>
</section><!-- .no-results -->
<?php
do_action( 'woocommerce_after_lost_password_form' );
?>
<style>
@media (max-width:768px){
.woocommerce-form-row.woocommerce-form-row--first.form-row.form-row-first{
	
		width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
	}
}
#user_login{
	width: 40%;
}
@media (max-width:768px){
	#user_login{
	
		width: 100%;
	}
}

</style>