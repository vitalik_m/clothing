<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */
global $product;
$product_id = get_the_ID();
if($_POST && !empty($_POST['comment_product'])){
	insert_comment_product($_POST['comment_product'], $product_id);
	// echo '<pre>'; cl_print_r($_POST['comment_product']); echo '</pre>';	
}
defined( 'ABSPATH' ) || exit;
$settings = $GLOBALS['settings'];

$url_urich = get_url_for_file('img'); 
//cl_print_r($product);
//$product = wc_get_product($product_id );
$link = get_permalink($product_id );
$url_urich = get_url_for_file('img'); 
$product_name = $product->get_name();
$symbol = get_woocommerce_currency_symbol();
            $price = $product->get_regular_price() . ' ' .$symbol;
if (has_post_thumbnail()) $product_image = get_the_post_thumbnail_url($product_id , 'medium');
$color = $product->get_attribute('pa_color');
$color = explode(', ', $color);
            $size = $product->get_attribute('pa_size');   
            $size = explode(', ', $size);
            $product_image_gallery = $product->get_gallery_image_ids( 'view' );

            $attachments         = array_filter( $product_image_gallery );
            $update_meta         = false;
            $updated_gallery_ids = array();
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$rate = intval(get_field('rate'));
?>
    <section class="destiny-wrapper">
        <div class="destiny">
            <?php echo do_action('breadcrumb'); ?>
        </div>
    </section>

	<section class="wrapper-item-card">
        <div class="item-card">
			<div class="item-card-title">

				<div class="item-card-title">
					<p class="item-card-title-text"><?php echo $product_name; ?></p>
				</div>

				<div class="selected-item-line-info-rate">
					<?php for($i = 1; $i < 6; $i++){

						if($i <= $rate){ ?>
							<i class="fa fa-star gold" aria-hidden="true"></i>
						<?php } else { ?>
							<i class="fa fa-star-o bronze" aria-hidden="true"></i>
						<?php }
					}?>
				</div>

				<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
					<?php do_action( 'woocommerce_before_single_product_summary' ); ?>

						<div class="summary entry-summary">
							<?php	do_action( 'woocommerce_single_product_summary' ); ?>
						</div>
                        <input type="hidden" id="<?php echo $product->get_regular_price(); ?>" class="get_price_by_product">

                    <?php do_action( 'woocommerce_after_single_product_summary' ); ?>
				</div>
			</div>
		</div>
    </section>

<?php //do_action( 'woocommerce_after_single_product' ); ?>

<?php
	$text = get_the_content();
	//$product_desing = the_excerpt();
	// cl_print_r($product_desing);
?>

<section class="wrapper-description">
      <h2 class="title-h2">Description</h2>
	  <div class="description-product">
	  <?php echo $text; ?>
	  </div>
<!--	  <div class="description-product-table">-->
<!--	  --><?php //the_excerpt(); ?>
<!--	  </div>-->
</section>

<section  class="wrapper-review">
    <h2 class="title-h2">reviews</h2>
    <div class="review-slider">
        <?php get_comment_by_prod($product_id) ?>
    </div>
    <div id="js_review_slider" class="tab_page_counter">
        <?php echo do_action('Paginator_main'); ?>
    </div>

    <?php if(is_user_logged_in()) { ?>
        <div class="add-btn" id="add_review">add a review</div>
    <?php } else { ?>
        <a class="add-btn" href="<?php echo get_home_url(); ?>/log-in/?replace=<?php echo $_SERVER['REQUEST_URI'] ?>">add a review</a>
    <?php } ?>
</section>

<section class="wrapper-suggestion">
    <div class="suggestion">
        <h2 class="title-h2">customers also bought</h2>
        <div id="customers_also_bought" class="tab-selected">
            <?php
            $best_sell_products_query = query_posts([
                'post_type' => 'product',
                'post_status' => 'publish',
                 'posts_per_page' => '1000',
                'orderby' => 'post_modified'
              ]);
            if ($best_sell_products_query) {
                echo do_action('ProductSlider', $best_sell_products_query);
            }
            ?>
        </div>
        <div id="js_customers_also_bought" class="tab_page_counter">
            <?php echo do_action('Paginator_main'); ?>
        </div>
    </div>
</section>

<section>
    <form action="" method="post">
        <div class="comment_input" style="display: none; padding: 0 0 150px;">
            <h2 id="text-line-comment">Leave a comment</h2>
            <textarea name="comment_product" id="textarea-comment" cols="30" rows="10"></textarea>
            <input type="submit" class="add-btn" value="Leave a review"/>
        </div>
    </form>
</section>


<style>
    .wrapper-description{
        padding: 50px 0 50px;
        position: relative;
    }
    .description-product{
        display: flex;
        justify-content: space-between;
        flex-wrap: wrap;
        position: relative;
        width: 50%;
    }
    .description-product-table{
        display: flex;
        justify-content: space-between;
        flex-wrap: wrap;
        position: relative;
        width: 50%;
    }

</style>