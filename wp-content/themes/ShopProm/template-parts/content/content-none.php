<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

<section class="no-results not-found error-wrapper">
<div class="wrapper error">
          <p class="error-text">Nothing Found</p>
	<!-- <header class="page-header">
		<h1 class="page-title"><?php _e( 'Nothing Found', 'twentynineteen' ); ?></h1>
	</header>.page-header -->

	<div class="page-content">
		
		<p>Sorry, but nothing matched your search terms. Please try again with some different keywords.</p>
	
	<?php get_search_form(); ?>
	</div><!-- .page-content -->
	</div>
</section><!-- .no-results -->
