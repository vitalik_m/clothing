<?php // Template Name: Page Shop?>

<?php 
 if($_GET){
  // cl_print_r($_GET);

      if($_GET['select'] == 'by date (new first)'){
          $best_sell_products_query = query_posts([
            'post_type' => 'product',
            'post_status' => 'publish',
            'posts_per_page' => '-1',
            'order' => 'ASC'
          ]);
      }
      if($_GET['select'] == 'by date (old first)'){
        $best_sell_products_query = query_posts([
          'post_type' => 'product',
          'post_status' => 'publish',
          'posts_per_page' => '-1',
          'order' => 'DESC'
        ]);
    }
    if($_GET['select'] == 'by change date'){
      $best_sell_products_query = query_posts([
        'post_type' => 'product',
        'post_status' => 'publish',
        'posts_per_page' => '-1',
        'orderby' => 'post_modified'
      ]);
  }
  if($_GET['select'] == 'by title'){
    $best_sell_products_query = query_posts([
      'post_type' => 'product',
      'post_status' => 'publish',
      'posts_per_page' => '-1',
      'orderby' => array( 'title' => 'DESC', 'menu_order' => 'ASC' )
    ]);
}
if($_GET['select'] == 'by price (cheap at first)'){
  $best_sell_products_query = query_posts([
    'post_type' => 'product',
      'post_status' => 'publish',
      'posts_per_page' => '-1',
    'orderby' => 'meta_value_num', 
    'meta_key' => '_price',
    'order' => 'ASC'
  ]);
}
if($_GET['select'] == 'by price (expensive first)'){
  $best_sell_products_query = query_posts([
    'post_type' => 'product',
      'post_status' => 'publish',
      'posts_per_page' => '-1',
    'orderby' => 'meta_value_num', 
    'meta_key' => '_price',
    'order' => 'DESC'
  ]);
}
if($_GET['select'] == 'by rate'){
  $best_sell_products_query = query_posts([
    'post_type' => 'product',
      'post_status' => 'publish',
      'posts_per_page' => '-1',
    'orderby' => 'meta_value_num', 
    'meta_key' => 'rate'
  ]);
}
 }
?>
<?php get_header(); ?>
<main>
<?php // get_sidebar('sidebar-1'); 

$modeType = 'block';
		if(isset($_COOKIE['prod-type-view']) && $_COOKIE['prod-type-view'] == 'list'){
				$modeType = 'list';
		}  
?>
      <section class="wrapper-filter">
        <div class="filter wrapper">
          <?php  sub_menu_filter(); ?>  
          <?php //echo sub_menu_other_filter(); ?>
        </div> 
        <div id="js_filter_more" class="filter wrapper" style="display: none">
          <?php  sub_menu_filter(); ?>  
        </div>
      </section>
      <section class="wrapper-pagination">
        <div class="pagination wrapper">
            <?php echo do_action('breadcrumb'); ?>
          <div class="pagination-stuff">

          <div class="pagination-margin pagination-stuff-sortby">
              <p class="pagination-stuff-sortby-text">sort by:</p>
              <select class="sort-select" name="" id="sort">
                <option selected value="select">Select</option>
                <option value="by date (new first)">by date (new first)</option>
                <option value="by date (old first)">by date (old first)</option>
                <option value="by change date">by change date</option>
                <option value="by title">by title</option>
                <option value="by rate">by rate</option>
                <option value="by price (cheap at first)">by price (cheap at first)</option>
                <option value="by price (expensive first)">by price (expensive first)</option>
              </select>

            </div>
            <div class="pagination-margin pagination-stuff-page-number">
            <?php //echo do_action('Paginator_main'); ?>
            </div>

          <?php 
          // cl_print_r($modeType);
          ?>
            <div class="pagination-margin pagination-stuff-list">
              <div class="list-swap-wrapper active <?php if($modeType != 'block')  echo 'cust-no-active'?>" id="block" onclick="filterMode(this,  'block')">
                <i class="fa fa-th" aria-hidden="true"></i></div>
              <div class="list-swap-wrapper active <?php if($modeType == 'block')  echo 'cust-no-active'  ?>" id="list"  onclick="filterMode(this,  'list')">
                <i class="fa fa-list-ul" aria-hidden="true"></i></div>
            </div>

          </div>
        </div>
      </section>

      <section class="wrapper-bigcatalogue">
        <div class="bigcatalogue wrapper">
          <div id="bigcatalogue_js" class="tab-selected bigcatalogue-categories">
           
          <?php echo do_action('ClothingCat'); ?>
            
          </div>
          <div id="js_bigcatalogue" class="tab_page_counter">
          <?php echo do_action('Paginator_main'); ?>
          </div>
        </div>
      </section>
      <section class="wrapper-plate">

        <div class="plate wrapper">
        
        
        <?php 
        if($best_sell_products_query){
          echo do_action('ProductSliderShop',$best_sell_products_query);
        }
        else{
        $best_sell_products_query = query_posts([
                    'post_type' => 'product',
                    'post_status' => 'publish',
                    'posts_per_page' => '-1',
                    'orderby' => 'post_modified'
                  ]);

                if ($best_sell_products_query): ?>
                            <?php echo do_action('ProductSliderShop',$best_sell_products_query); ?>
                <?php endif; 
        }
                ?>
         
        </div>
        <div class="showmore" style="display: none">
          <div id="showmore-button" class="showmore-btn">show more</div>
        </div>

      </section>
    </main>

   

<?php get_footer(); ?>