<?php // Template Name: Page Thank you page?>

<?php get_header(); ?>
<?php $url_urich = get_url_for_file('img'); ?>
<main>
      <section class="thanks-wrapper">
        <div class="wrapper thanks">
          <div class="thanks-img-container">
            <img class="thanks-img" src="<?php echo $url_urich; ?>assets/micro-lite_softshell_1.png" alt="thanks-pic" />
          </div>
          <div class="thanks-container">
            <p class="thanks-title">Thank you!</p>
            <p class="thanks-subtitle">Your order is accepted</p>
            <a href="<?php echo get_home_url(); ?>" class="add-btn">back to main</a>
          </div>
        </div>
        <div id="empty-div-thankyou-page"></div>
      </section>
    </main>

<?php get_footer(); ?>