<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
$url_urich = get_url_for_file('img'); 
get_header();
?>

<main>
      <section class="error-wrapper">
        <div class="wrapper error">
          <p class="error-text">Page not found</p>
          <div class="error-img-wrapper">
            <img class="error-img" src="<?php echo $url_urich; ?>assets/404.png" alt="404" />
          </div>
          <a href="<?php echo get_home_url(); ?>" class="add-btn">back to main</a>
        </div>
      </section>
    </main>

<?php
get_footer();
