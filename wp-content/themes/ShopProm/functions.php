<?php
/**
 * Twenty Nineteen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

/**
 * Twenty Nineteen only works in WordPress 4.7 or later.
 */


/*------------------------------------*\
	Theme Support
\*------------------------------------*/


if ( version_compare( $GLOBALS['wp_version'], '4.7', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

add_action( 'admin_enqueue_scripts', function(){
	wp_enqueue_script( 'my-wp-admin', get_template_directory_uri() .'/js/my-admin.js');
}, 99 );

if ( ! function_exists( 'twentynineteen_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function twentynineteen_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Twenty Nineteen, use a find and replace
		 * to change 'twentynineteen' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'twentynineteen', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		add_theme_support('post-thumbnails');
		add_image_size('large', 700, '', true); // Large Thumbnail
		add_image_size('medium', 250, '', true); // Medium Thumbnail
		add_image_size('small', 120, '', true); // Small Thumbnail
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 1568, 9999 );

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus(
			array(
				'menu-1' => __( 'Primary', 'twentynineteen' ),
				'footer' => __( 'Footer Menu', 'twentynineteen' ),
				'social' => __( 'Social Links Menu', 'twentynineteen' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 190,
				'width'       => 190,
				'flex-width'  => false,
				'flex-height' => false,
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Add support for Block Styles.
		add_theme_support( 'wp-block-styles' );

		// Add support for full and wide align images.
		add_theme_support( 'align-wide' );

		// Add support for editor styles.
		add_theme_support( 'editor-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style-editor.css' );

		// Add custom editor font sizes.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name'      => __( 'Small', 'twentynineteen' ),
					'shortName' => __( 'S', 'twentynineteen' ),
					'size'      => 19.5,
					'slug'      => 'small',
				),
				array(
					'name'      => __( 'Normal', 'twentynineteen' ),
					'shortName' => __( 'M', 'twentynineteen' ),
					'size'      => 22,
					'slug'      => 'normal',
				),
				array(
					'name'      => __( 'Large', 'twentynineteen' ),
					'shortName' => __( 'L', 'twentynineteen' ),
					'size'      => 36.5,
					'slug'      => 'large',
				),
				array(
					'name'      => __( 'Huge', 'twentynineteen' ),
					'shortName' => __( 'XL', 'twentynineteen' ),
					'size'      => 49.5,
					'slug'      => 'huge',
				),
			)
		);

		// Editor color palette.
		add_theme_support(
			'editor-color-palette',
			array(
				array(
					'name'  => __( 'Primary', 'twentynineteen' ),
					'slug'  => 'primary',
					'color' => twentynineteen_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 33 ),
				),
				array(
					'name'  => __( 'Secondary', 'twentynineteen' ),
					'slug'  => 'secondary',
					'color' => twentynineteen_hsl_hex( 'default' === get_theme_mod( 'primary_color' ) ? 199 : get_theme_mod( 'primary_color_hue', 199 ), 100, 23 ),
				),
				array(
					'name'  => __( 'Dark Gray', 'twentynineteen' ),
					'slug'  => 'dark-gray',
					'color' => '#111',
				),
				array(
					'name'  => __( 'Light Gray', 'twentynineteen' ),
					'slug'  => 'light-gray',
					'color' => '#767676',
				),
				array(
					'name'  => __( 'White', 'twentynineteen' ),
					'slug'  => 'white',
					'color' => '#FFF',
				),
			)
		);

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );
	}
endif;
add_action( 'after_setup_theme', 'twentynineteen_setup' );

//cl_print_r(admin_url('admin-ajax.php'));
// AJAX
add_action( 'wp_enqueue_scripts', 'myajax_data', 99 );
function myajax_data(){
	wp_localize_script('my_common', 'myajax', 
		array(
			'ajax_url' => admin_url('admin-ajax.php'),
			'ajax_nonce' => wp_create_nonce('myajax-nonce')
		)
	);  
//	cl_print_r(admin_url('admin-ajax.php'));
}

add_action( 'wp_ajax_bild_prod_view_sort', 'bild_prod_view_sort_filter' );
add_action('wp_ajax_nopriv_bild_prod_view_sort', 'bild_prod_view_sort_filter');

function bild_prod_view_sort_filter(){
	$sort = $_POST['sort_name'];
	//print_r($sort);
	foreach($sort as $sort_filter){
		echo $sort_filter;
	}

}


add_action( 'wp_ajax_add_new_price_design', 'add_new_price_design_func' );
add_action('wp_ajax_nopriv_add_new_price_design', 'add_new_price_design_func');

function add_new_price_design_func(){

	$price_post = $_POST['arr_price'];
	$post_id = $_POST['post_url'];
	$price_post_separated = implode(",", $price_post);
	 //print_r($price_post_separated);
	// print_r($_POST['post_url']);

	update_post_meta($post_id, '_price_design', $price_post);
}

function get_new_price_design_func($id_prod){
	$values = get_post_meta( $id_prod, '_price_design' );
	foreach($values as $val){
		$result = $val;
	}
	
	return $result;
}


add_action( 'wp_ajax_get_state_mark', 'get_state_marks' );
add_action('wp_ajax_nopriv_get_state_mark', 'get_state_marks');

function get_state_marks(){
	$user_id = get_current_user_id();
	$product_id = $_POST['product_id'];
	$bool_state = $_POST['state'];
	global $wpdb;
	$result = $wpdb->get_results("SELECT `meta_value` FROM `wp_usermeta` WHERE `user_id`='$user_id' AND `meta_key`='bookmark_arr'");
	foreach($result as $value){
		$stack = $value->meta_value;
		
	}
	if($bool_state == 'true'){
		if(strlen($stack) == 0){
			$stack = $product_id;
		}
		else{
				$stack .= ', '.$product_id;			
		}
		$stack_array = explode(", ", $stack); 
		$stack_result = array_unique($stack_array);
		$stack = implode(", ", $stack_result); 
	}
		else{
			$stack_array = explode(", ", $stack); 
			$stack_result = array_unique($stack_array);
			unset( $stack_result[array_search( $product_id, $stack_result )] );
			$stack = implode(", ", $stack_result); 
		}

	//print_r($stack);
	update_user_meta( $user_id, 'bookmark_arr', $stack );

	
	$mark = get_array_bookmarks();
	$best_sell_products_query = query_posts([
		'post__in' => $mark,
		'post_type' => 'product',
		'post_status' => 'publish',
		
		'orderby' => 'post_modified'
		]);

			if ($best_sell_products_query): 
						echo do_action('ProductBookmarks',$best_sell_products_query); 
				endif; 
	die();
}

function get_array_bookmarks(){
	$user_id = get_current_user_id();
	global $wpdb;
	$result = $wpdb->get_results("SELECT `meta_value` FROM `wp_usermeta` WHERE `user_id`='$user_id' AND `meta_key`='bookmark_arr'");
	foreach($result as $value){
		$stack = $value->meta_value;
	}
	$arr = explode(", ", $stack);
	$result = array_unique($arr);
	//cl_print_r($result);
	return $result;
}

add_action( 'wp_ajax_bild_cart_tab', 'bild_cart_tabul' );
add_action('wp_ajax_nopriv_bild_cart_tab', 'bild_cart_tabul');

function bild_cart_tabul(){
	
 }


add_action( 'wp_ajax_bild_prod_view', 'bild_product_page' );
add_action('wp_ajax_nopriv_bild_prod_view', 'bild_product_page');

function bild_product_page(){
	global $wpdb;

	$fiter_item = $_POST['filter_name'];
	$res = $wpdb->get_results("SELECT * FROM `wp_terms` WHERE `name`='$fiter_item'");
	foreach($res as $val){
		$val_id = $val->term_id;
		$res_tax = $wpdb->get_results("SELECT `taxonomy` FROM `wp_term_taxonomy` WHERE `term_id`='$val_id'");
		$value_slug = $val->slug;
	}
		foreach($res_tax as $value){
			$value_taxonomy = $value->taxonomy;
		}
		//	print_r($res_tax);
			$best_sell_products_query = query_posts([
					'post_type' => 'product',
					'post_status' => 'publish',
					'posts_per_page' => '10000',
					'orderby' => 'post_modified',
					'tax_query' => array( array(
						'taxonomy' => $value_taxonomy, // Product attribute taxonomy: always start with 'pa_'
						'field'    => 'slug', // Can be 'term_id', 'slug' or 'name'
						'terms'    => array($value_slug),
				), )
				// 'post__in' => array(117, 67, 66),
				]);

			if ($best_sell_products_query): 
			echo do_action('ProductSliderShop',$best_sell_products_query); 
			endif; 
			die();
}

add_action( 'wp_ajax_add_to_cart_prod', 'add_to_cart_products' );
add_action('wp_ajax_nopriv_add_to_cart_prod', 'add_to_cart_products');

function add_to_cart_products(){

	global $post, $woocommerce;
		$product_id = $_POST['id_prod'];
	$product = wc_get_product($product_id);
	
	$korzina = new WC_Cart();
	$woocommerce->cart->add_to_cart( $post->ID );
	$input_s = $_POST['value_input_s'];
	$input_m = $_POST['value_input_m'];
	$input_l = $_POST['value_input_l'];
	$input_xl = $_POST['value_input_xl'];
	$input_xxl = $_POST['value_input_xxl'];
	$input_3xl = $_POST['value_input_3xl'];

  $product->add_to_cart_url();

	//$cart = $woocommerce->cart->get_cart();
	//print_r($woocommerce);
		//$woocommerce->cart->add_to_cart( $product_id );

	//$korzina->add_to_cart( $prod_id, $input_s);
	// if($input_s){ $korzina->add_to_cart( $prod_id, $input_s, 51); }
	// if($input_m){ $korzina->add_to_cart( $prod_id, $input_m, 52); }
	// if($input_l){ $korzina->add_to_cart( $prod_id, $input_l, 31); }
	// if($input_xl){ $korzina->add_to_cart( $prod_id, $input_xl, 53); }
	// if($input_xxl){ $korzina->add_to_cart( $prod_id, $input_xxl, 30); }
	// if($input_3xl){ $korzina->add_to_cart( $prod_id, $input_3xl, 54); }
	die();
}



/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function twentynineteen_widgets_init() {

	register_sidebar(
		array(
			'name'          => __( 'Footer', 'twentynineteen' ),
			'id'            => 'sidebar-1',
			'description'   => __( 'Add widgets here to appear in your footer.', 'twentynineteen' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

}
add_action( 'widgets_init', 'twentynineteen_widgets_init' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width Content width.
 */
function twentynineteen_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'twentynineteen_content_width', 640 );
}
add_action( 'after_setup_theme', 'twentynineteen_content_width', 0 );

/**
 * Enqueue scripts and styles.
 */
function twentynineteen_scripts() {
	wp_enqueue_style( 'twentynineteen-style', get_stylesheet_uri(), array(), wp_get_theme()->get( 'Version' ) );

	wp_style_add_data( 'twentynineteen-style', 'rtl', 'replace' );

	if ( has_nav_menu( 'menu-1' ) ) {
		wp_enqueue_script( 'twentynineteen-priority-menu', get_theme_file_uri( '/js/priority-menu.js' ), array(), '1.1', true );
		wp_enqueue_script( 'twentynineteen-touch-navigation', get_theme_file_uri( '/js/touch-keyboard-navigation.js' ), array(), '1.1', true );
	}

	wp_enqueue_style( 'twentynineteen-print-style', get_template_directory_uri() . '/print.css', array(), wp_get_theme()->get( 'Version' ), 'print' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'twentynineteen_scripts' );

/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function twentynineteen_skip_link_focus_fix() {
	// The following is minified via `terser --compress --mangle -- js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'twentynineteen_skip_link_focus_fix' );

/**
 * Enqueue supplemental block editor styles.
 */
function twentynineteen_editor_customizer_styles() {

	wp_enqueue_style( 'twentynineteen-editor-customizer-styles', get_theme_file_uri( '/style-editor-customizer.css' ), false, '1.1', 'all' );

	if ( 'custom' === get_theme_mod( 'primary_color' ) ) {
		// Include color patterns.
		require_once get_parent_theme_file_path( '/inc/color-patterns.php' );
		wp_add_inline_style( 'twentynineteen-editor-customizer-styles', twentynineteen_custom_colors_css() );
	}
}
add_action( 'enqueue_block_editor_assets', 'twentynineteen_editor_customizer_styles' );

/**
 * Display custom color CSS in customizer and on frontend.
 */
function twentynineteen_colors_css_wrap() {

	// Only include custom colors in customizer or frontend.
	if ( ( ! is_customize_preview() && 'default' === get_theme_mod( 'primary_color', 'default' ) ) || is_admin() ) {
		return;
	}

	require_once get_parent_theme_file_path( '/inc/color-patterns.php' );

	$primary_color = 199;
	if ( 'default' !== get_theme_mod( 'primary_color', 'default' ) ) {
		$primary_color = get_theme_mod( 'primary_color_hue', 199 );
	}
	?>

	<style type="text/css" id="custom-theme-colors" <?php echo is_customize_preview() ? 'data-hue="' . absint( $primary_color ) . '"' : ''; ?>>
		<?php echo twentynineteen_custom_colors_css(); ?>
	</style>
	<?php
}
add_action( 'wp_head', 'twentynineteen_colors_css_wrap' );

/**
 * SVG Icons class.
 */
require get_template_directory() . '/classes/class-twentynineteen-svg-icons.php';

/**
 * Custom Comment Walker template.
 */
require get_template_directory() . '/classes/class-twentynineteen-walker-comment.php';

/**
 * Enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * SVG Icons related functions.
 */
require get_template_directory() . '/inc/icon-functions.php';

/**
 * Custom template tags for the theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

require get_template_directory() . '/inc/function_customer.php';

add_action('ClothingCat','ClothingCategory');
add_action('ProductSliderShop','getProductsSliderShop');
add_action('ProductBookmarks','getProductBookmarks');
add_action('ProductSlider','getProductsSlider');
add_action('ProductSliderTag','getProductsSliderTag');
add_action('MainBanner','getMainBanner');
add_action('Paginator_main','getPaginatorInMain');
add_action('Add_Brands_main','getBrands');
add_action('breadcrumb','breadcrumb_function');
add_action('section_promo','get_section_promo', 10, 2);

add_filter( 'get_user_data_account_page', 'get_user_data_account', 1, 1 );

function insert_comment_product($comment, $id_product){
	
	$user_id = get_current_user_id();
	$user = wp_get_current_user();
	$time = current_time('mysql');
	$time_gmt = current_time('mysql',1);

global $wpdb;
	//   echo '<pre>'; cl_print_r($user); echo '</pre>';
	//  echo '<pre>'; cl_print_r($time_date); echo '</pre>';
	// echo '<pre>'; cl_print_r($user_id); echo '</pre>';
	$wpdb->insert(
		'wp_comments',
		array(  'comment_post_ID' => $id_product, 
				'comment_author' => $user->data->user_login, 
				'comment_author_email' => $user->data->user_email, 
				'comment_date' => $time, 
				'comment_date_gmt' => $time_gmt, 
				'comment_content' => $comment, 
				'comment_approved' => 0, 
				'user_id' => $user_id ),
		array( '%d', '%s', '%s', '%s', '%s', '%s',  '%d', '%d',)
	);
}

function get_phone_header(){
	$posts = get_posts( array(
		'numberposts' => -1,
		'post_status' => 'publish',
		'post_type'   => 'header'
	) ); 
		foreach( $posts as $post ){
			$phone = get_post_meta($post->ID, 'telephon');
		} wp_reset_postdata(); // сброс 
	return $phone;
}
function get_schedule_header(){
	$posts = get_posts( array(
		'numberposts' => -1,
		'post_status' => 'publish',
		'post_type'   => 'header'
	) ); 
		foreach( $posts as $post ){
			$schedule = get_post_meta($post->ID, '_schedule');
		} wp_reset_postdata(); // сброс 
	return $schedule;
}
function get_image_header(){
	global $wpdb;
	$posts = get_posts( array(
		'numberposts' => -1,
		'post_status' => 'publish',
		'post_type'   => 'header'
	) ); 
		foreach( $posts as $post ){
			// $category_thumbnail = get_the_post_thumbnail($post->ID, 'full');
			// $image = wp_get_attachment_url($category_thumbnail);
			$post_id = get_post_meta($post->ID, 'logo');
			//cl_print_r($post_id);
			$img_url = $wpdb->get_results("SELECT `guid` FROM `wp_posts` WHERE `ID`=237");
			//cl_print_r($img_url);
			foreach($img_url as $val){
				//cl_print_r($val->guid);
				return $val->guid;
			}
		 } wp_reset_postdata(); // сброс 
		
	
}


function get_section_promo($data_cat, $data_class){
	if ( have_posts() ) : 
		query_posts($data_cat); 
		   while (have_posts()) : the_post(); 
		   $posttags = get_the_tags();
			 if( $posttags ){
			   foreach( $posttags as $tag ){
						$arr_params = array( 'block' => $tag->name);
			   }
			 }
			$text = get_the_content();
			 preg_match("|<p>(.*)</p>|is", $text, $result);

			 if(isset($data_cat)){
				if($data_cat == 'p=159')
					$arr_params = array( 'block' => 'discount');
				elseif($data_cat == 'p=165')
					$arr_params = array( 'block' => 'post_modified');
				elseif($data_cat == 'p=167')
					$arr_params = array( 'block' => 'topsellers');
		   }
			if($data_cat == 'p=194'){
				?>
				 <div class="promo-block-big">
					<div class="promo-block-big-img-wrap"><?php echo get_the_post_thumbnail( get_the_ID(), array(210, 210)); ?></div>
						<div class="promo-block-big-p-wrap">
							<p class="promo-block-big-p"><?php echo $result[1]; ?></p>
						</div>
				</div>
				   <?php
			} else{
		   ?>
		 <a href="<?php echo  add_query_arg( $arr_params, get_home_url().'/category/' ); ?>"
	   ><div class="promo-block-small <?php echo $data_class; ?>">
		 <p class="promo-block-small-p"><?php echo $text; ?></p>
		 <div class="promo-block-small-img"><?php echo get_the_post_thumbnail( get_the_ID(), 'normal'); ?></div></div
	 ></a>
			<?php  }   endwhile; 
	   endif; 
	 wp_reset_query(); 

}

function get_user_data_account($user_id){
	global $wpdb;
	$res_location = $wpdb->get_results("SELECT `meta_value` FROM `wp_usermeta` WHERE `user_id`='$user_id' AND `meta_key`='_location_user'");
	$res_phone = $wpdb->get_results("SELECT `meta_value` FROM `wp_usermeta` WHERE `user_id`='$user_id' AND `meta_key`='_mobile_phone'");
	$res_name = $wpdb->get_results("SELECT `meta_value` FROM `wp_usermeta` WHERE `user_id`='$user_id' AND `meta_key`='first_name'");
	$res_last_name = $wpdb->get_results("SELECT `meta_value` FROM `wp_usermeta` WHERE `user_id`='$user_id' AND `meta_key`='last_name'");
	 foreach($res_location as $value){
	  $user_location = $value->meta_value;
	   }
	   foreach($res_phone as $value){
		$user_phone = $value->meta_value;
		 }
		 foreach($res_name as $value){
		  $user_name = $value->meta_value;
		   }
		   foreach($res_last_name as $value){
			$user_last_name = $value->meta_value;
			 }
	$result = array('user_name' => $user_name,
					'user_lastname' => $user_last_name,
					'user_phone' => $user_phone,
					'user_country' => $user_location);
			 return $result;
}

function breadcrumb_function(){
	get_template_part('templates/breadcrumb');
}

function ClothingCategory(){

		  $args2 = array(
			'taxonomy'     => 'product_cat',
			'child_of'     => 0,
			'parent'       => 52,
			'orderby'      => 'name',
			'hierarchical' => 1,
			'title_li'     => '',
			'hide_empty'   => 0
		  );
		  $sub_cats = get_categories( $args2 );
		  if($sub_cats) {
			//cl_print_r($sub_cats);
			  foreach($sub_cats as $sub_category) {
				$category_thumbnail = get_woocommerce_term_meta($sub_category->term_id, 'thumbnail_id', true);
				$image = wp_get_attachment_url($category_thumbnail);
				 // cl_print_r($sub_category->name); ?>
				 <a href="" class="a-categorie" id_category="<?php echo $sub_category->term_id; ?>" onclick="filterCategory(this)">
				 <div class="categorie">
				 <div class="categorie-img">
					 <img src="<?php echo $image; ?>" alt="<?php echo $sub_category->name; ?>" /></div>
				 <p class="categorie-p"><?php echo $sub_category->name; ?></p>
			   </div></a>
			 <?php 
			 }
		  } 
}

function get_result_by_category($type){
	global $wpdb;
	 $res = $wpdb->get_results("SELECT * FROM `wp_posts` WHERE `post_type`= 'product' AND `post_title` LIKE '%$type%'");
	// cl_print_r($res);
}

function getBrands(){

	$posts = get_posts( array(
		'numberposts' => -1,
		'post_status' => 'publish',
		'post_type'   => 'brands'
	) ); 
	foreach( $posts as $post ){
         ?>
			<?php  //cl_print_r($post); ?>
				<div class="tab-selected-item" style="width: 180px; height: 90px; margin: 0 5px">
			    	<?php echo get_the_post_thumbnail($post->ID, array(152, 167)); ?>
			    </div>
	   <?php
  } wp_reset_postdata(); // сброс 
}

function getPaginatorInMain(){
?>
 <span class="counter-first">1</span><span class="counter-dash"> / </span
                ><span class="counter-sum">1</span>
<?php
}


function getMainBanner($query_products){
	$posts = get_posts( array(
		'category'    => 44,
		'post_type'   => 'post'
	) ); 
	$i=0;
	foreach( $posts as $post ){
		if($post && $i<6){
				$text = get_the_content();
			 preg_match("|<p>(.*)</p>|is", $post->post_content, $result); ?>
		<?php //cl_print_r($result); ?>
				<div class="baner">
					<div class="catalog-wrapper">
						<div class="catalog-text">
							<p class="catalog-text-p"><?php echo $result[1]; ?></p>
							<a class="catalog-text-button" href="<?php echo get_home_url(); ?>/category/">View Catalog</a>
						</div>
						<div class="catalog"><?php echo get_the_post_thumbnail($post->ID, 'attachment'); ?></div>
					</div>
				</div>
	   <?php
	   $i++;
		}
	} wp_reset_postdata(); // сброс 

}

function getProductsSliderShop($query_products){
	if(is_array($query_products)):

		$modeType = 'block';
		if(isset($_COOKIE['prod-type-view']) && $_COOKIE['prod-type-view'] == 'list'){
				$modeType = 'list';
		}  
			if($modeType == 'block'){
				get_template_part('templates/products-view');
					echo '<a href="#" class="flex-empty"></a>
								<a href="#" class="flex-empty"></a>
								<a href="#" class="flex-empty"></a>
								<a href="#" class="flex-empty"></a>
								<a href="#" class="flex-empty"></a>
								<a href="#" class="flex-empty"></a>';
			}
			else{
		//		cl_print_r('222');
			get_template_part('templates/products-view-line');
					echo '<a href="#" class="flex-empty-line"></a>';
			}
		endif; ?>


		<?php
}

function getProductsSlider($query_products){
	if(is_array($query_products)):
			get_template_part('templates/products-view');
    endif;
}

function getProductsSliderTag($query_products){
	if(is_array($query_products)):
			get_template_part('templates/products-view_tag');
    endif;
}

function getProductBookmarks($query_products){
	if(is_array($query_products)):
		get_template_part('templates/products-view-bookmarks');
		echo '<a href="#" class="flex-empty-line"></a>';
endif;
}

function get_url_for_file($type){
	$url_urich1 = '/inc/Urich/'; 
	$url_urich2 = get_template_directory_uri(); 
	$url_urich = $url_urich2 . $url_urich1; 
		if($type == 'img'){
			return $url_urich;
		}
	}

	function get_pagination_page(){
		return twentynineteen_the_posts_navigation();
	}

add_action( 'wp_ajax_filter_product_shop_page', 'filter_product_shop_page_func' );
 add_action('wp_ajax_nopriv_filter_product_shop_page', 'filter_product_shop_page_func');

 	function filter_product_shop_page_func(){
		 global $wpdb;

		
		$term_id = $_POST['id_cat'];
		$term_slug = $wpdb->get_results("SELECT `slug` FROM `wp_terms` WHERE `term_id`='$term_id'");
		foreach($term_slug as $val){
			$term_slug = $val->slug;
		}
		
		$products_query = query_posts([
			'post_type' => 'product',
			'post_status' => 'publish',
			'posts_per_page' => '10000',
			'orderby' => 'post_modified',
			'product_cat' => $term_slug
		  ]);

		if ($products_query): 
		 		 echo do_action('ProductSliderShop',$products_query); 
		  endif; 
		  die();
	 }



 	add_action( 'wp_ajax_send_message_at_home', 'send_message_at_home_page' );
 add_action('wp_ajax_nopriv_send_message_at_home', 'send_message_at_home_page');

 	function send_message_at_home_page(){
 	
		$user_email = $_POST['input_send_mess'];
		$admin_email = get_option('admin_email');

	 $headers= "MIME-Version: 1.0\r\n";						
		$headers .= "Content-type: text/html; charset=utf-8\r\n";

			$message_send = '<hr>
							<p>User sent his e-post for consultation: '.  $user_email.'</p>
							<p><br /> </p>
							 </hr>';
		
				$topic_of_the_letter_send = "Message from site http://demoproject.company/";
			

		mail ($admin_email, $topic_of_the_letter_send, $message_send,$headers);
		die();

 	}

	function add_info_usermeta(){
	
		$userdata = array(			
				'user_pass'       => $_POST['password'], // обязательно
				'user_login'      => $_POST['account_email'], // обязательно
				'user_email'      => $_POST['account_email'],
				'first_name'      => $_POST['account_first_name'],
				'last_name'       => $_POST['account_last_name'],
			);	
		 $user_id = wp_insert_user( $userdata ); 
		 if( is_wp_error( $user_id ) ) {
		// cl_print_r($_POST['subcribe_newsletter']);
		cl_print_r('111111111');
		 }
				if($_POST['subcribe_newsletter'] == ''){
					add_user_meta($user_id, '_newsletter', 'false');
				}
				else{
					add_user_meta($user_id, '_newsletter', 'true');
				}

		return $user_id;
	}

	function get_pass_user($user_id){
		global $wpdb;
		$res = $wpdb->get_results("SELECT `user_pass` FROM `wp_users` WHERE `ID`='$user_id'");
		return $res;
	}

	function cl_print_r ($var, $label = '')
{
  $str = json_encode(print_r ($var, true));
  echo "<script>console.group('".$label."');console.log('".$str."');console.groupEnd();</script>";
}

// Store custom data in cart item
add_action( 'woocommerce_add_cart_item_data2','save_custom_data_in_cart', 20, 2 );

function save_custom_data_in_cart( $cart_item_data, $product_id ) {
//	cl_print_r($_REQUEST);
	foreach($_REQUEST as $value=>$key){
		if( isset( $_REQUEST[$value] ) && $key!=0 ){
				if(is_array($key)){
					foreach($key as $val){
						$cart_item_data[$value] = array(
							'label' => __('color'),
							'value' => esc_attr($val),
						);

					}
				}
				else
			$cart_item_data[$value] = array(
				'label' => __($value),
				'value' => esc_attr($key),
			);
				if( count($cart_item_data[$value]) > 0   )
			$cart_item_data[$value]['key'] = md5( microtime().rand() );
		}
	}
		//cl_print_r($cart_item_data);
		$result = apply_filters( 'woocommerce_add_cart_item_data', $cart_item_data,$product_id);
		$result2 = apply_filters('woocommerce_get_cart_item_from_session', $result, $result,'');
		//cl_print_r($result2);
    return $cart_item_data;
}


// Display item custom data in cart and checkout pages
add_filter( 'woocommerce_get_item_data', 'render_custom_data_on_cart_and_checkout', 20, 2 );

function render_custom_data_on_cart_and_checkout( $cart_data, $cart_item ){
    $custom_items = array();

    if( !empty( $cart_data ) )
        $custom_items = $cart_data;

    if( isset( $cart_item['size-3XL'] ) )
        $custom_items[] = array(
            'name'  => $cart_item['size-3XL']['label'],
            'value' => $cart_item['size-3XL']['value'],
        );
//	cl_print_r($custom_items);
    return $custom_items;
}


add_action('wp_ajax_wdm_add_user_custom_data_options', 'wdm_add_user_custom_data_options_callback');
add_action('wp_ajax_nopriv_wdm_add_user_custom_data_options', 'wdm_add_user_custom_data_options_callback');

function wdm_add_user_custom_data_options_callback()
{
      //Custom data - Sent Via AJAX post method
      $product_id = $_POST['id_prod']; //This is product ID
			$user_custom_data_values = array( 'size-s' => $_POST['value_input_s'],
			'size-m' => $_POST['value_input_m'],
			'size-l' => $_POST['value_input_l'],
			'size-xl' => $_POST['value_input_xl'],
			'size-xxl' => $_POST['value_input_xxl'],
			'size-3xl' => $_POST['value_input_3xl'] );
			 //This is User custom value sent via AJAX
      session_start();
			$_SESSION['wdm_user_custom_data'] = $user_custom_data_values;
			
			//print_r($_SESSION);
      die();
}

add_filter('woocommerce_add_cart_item_data','wdm_add_item_data',1,2);
 
if(!function_exists('wdm_add_item_data'))
{
    function wdm_add_item_data($cart_item_data,$product_id)
    {
			cl_print_r('start');
        /*Here, We are adding item in WooCommerce session with, wdm_user_custom_data_value name*/
        global $woocommerce;
        session_start();    
        if (isset($_SESSION['wdm_user_custom_data'])) {
			
            $option = $_SESSION['wdm_user_custom_data'];       
            $new_value = array('wdm_user_custom_data_value' => $option);
        }
        if(empty($option)){
			return $cart_item_data;
		}
        else
        {    
			//cl_print_r($_SESSION['wdm_user_custom_data']);
            if(empty($cart_item_data))
                return $new_value;
            else
                return array_merge($cart_item_data,$new_value);
		}
		
        unset($_SESSION['wdm_user_custom_data']); 
        //Unset our custom session variable, as it is no longer needed.
    }
}

add_filter('woocommerce_get_cart_item_from_session', 'wdm_get_cart_items_from_session', 1, 3 );
if(!function_exists('wdm_get_cart_items_from_session'))
{
    function wdm_get_cart_items_from_session($item,$values,$key)
    {
        if (array_key_exists( 'wdm_user_custom_data_value', $values ) )
        {
        $item['wdm_user_custom_data_value'] = $values['wdm_user_custom_data_value'];
        }       
        return $item;
    }
}

add_filter('woocommerce_checkout_cart_item_quantity','wdm_add_user_custom_option_from_session_into_cart',1,3);  
add_filter('woocommerce_cart_item_price','wdm_add_user_custom_option_from_session_into_cart',1,3);
if(!function_exists('wdm_add_user_custom_option_from_session_into_cart'))
{
 function wdm_add_user_custom_option_from_session_into_cart($product_name, $values, $cart_item_key )
    {
        /*code to add custom data on Cart & checkout Page*/    
        if(count($values['wdm_user_custom_data_value']) > 0)
        {
            $return_string = $product_name . "</a><dl class='variation'>";
            $return_string .= "<table class='wdm_options_table' id='" . $values['product_id'] . "'>";
            $return_string .= "<tr><td>" . $values['wdm_user_custom_data_value'] . "</td></tr>";
            $return_string .= "</table></dl>"; 
            return $return_string;
        }
        else
        {
            return $product_name;
        }
    }
}

add_action('woocommerce_add_order_item_meta','wdm_add_values_to_order_item_meta',1,2);
if(!function_exists('wdm_add_values_to_order_item_meta'))
{
  function wdm_add_values_to_order_item_meta($item_id, $values)
  {
        global $woocommerce,$wpdb;
        $user_custom_values = $values['wdm_user_custom_data_value'];
        if(!empty($user_custom_values))
        {
            wc_add_order_item_meta($item_id,'wdm_user_custom_data',$user_custom_values);  
        }
  }
}

add_action('woocommerce_before_cart_item_quantity_zero','wdm_remove_user_custom_data_options_from_cart',1,1);
if(!function_exists('wdm_remove_user_custom_data_options_from_cart'))
{
    function wdm_remove_user_custom_data_options_from_cart($cart_item_key)
    {
        global $woocommerce;
        // Get cart
        $cart = $woocommerce->cart->get_cart();
        // For each item in cart, if item is upsell of deleted product, delete it
        foreach( $cart as $key => $values)
        {
        if ( $values['wdm_user_custom_data_value'] == $cart_item_key )
            unset( $woocommerce->cart->cart_contents[ $key ] );
        }
    }
}

add_filter( 'woocommerce_checkout_fields' , 'new_woocommerce_checkout_fields', 10, 1 );
 
function new_woocommerce_checkout_fields($fields){
	
unset($fields['billing']['billing_company']);
unset($fields['billing']['billing_address_2']);
unset($fields['billing']['billing_state']);
unset($fields['order']['order_comments']);
unset($fields['account']['account_username']);
unset($fields['account']['account_password']);
unset($fields['account']['account_password-2']);


    
    return $fields;
}

add_filter("woocommerce_checkout_fields", "magik_new_billing_order_fields");
 
function magik_new_billing_order_fields($fields) {

	  $order = array(
		"billing_email", 
		"billing_phone",

		"billing_first_name", 
		"billing_last_name", 

		"billing_country",
		"billing_city",
		"billing_address_1", 
		"billing_postcode"		      
	);

	//cl_print_r($order);
	 foreach($order as $field)
	 {
		$ordered_fields[$field] = $fields["billing"][$field];
	 }

	$fields["billing"] = $ordered_fields;
	return $fields; 

}