<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @package proficiency
 */


do_action( 'neve_before_primary_end' );
?>


<?php do_action( 'neve_after_primary' ); ?>
<!--==================== proficicnecy-FOOTER AREA ====================-->
<footer>
<?php
$image_url = get_image_header();
$posts = get_posts( array(
		'numberposts' => -1,
		'post_status' => 'publish',
		'post_type'   => 'header'
	) ); 
	foreach( $posts as $post ){
    $phone = get_post_meta($post->ID, 'telephon');
    $schedule = get_post_meta($post->ID, '_schedule');
    $admin_email = get_option('admin_email');
    
   // cl_print_r($phone);
    ///cl_print_r($schedule);
         ?>
      <div class="footer wrapper">
      <div class="logotype">
            <img src="<?php echo $image_url; ?>" alt="logotype" />
          </div>
        <div class="navigation">
          <div class="navigation-icons">
            <i class="fa fa-phone" aria-hidden="true"></i>
          </div>
          <div class="navigation-info">
            <span class="navigation-info-working-time"><?php echo $schedule[0]; ?></span
            ><a class="navigation-info-mobile" href="tel:<?php echo $phone[0]; ?>"><?php echo $phone[0]; ?></a>
          </div>
        </div>
        <div class="navigation">
          <div class="navigation-icons">
            <i class="fa fa-envelope" aria-hidden="true"></i>
          </div>
          <div class="navigation-info">
            <a class="navigation-info-mobile mail-opacity" href="mailto:<?php echo $admin_email; ?>"
              ><?php echo $admin_email; ?></a
            >
          </div>
        </div>
        <div class="navigation">
          <div class="navigation-icons navigation-icons-black">

          <?php 
          $posts = get_posts( array(
            'posts_per_page' => '6',
            'numberposts' => -1,
            'post_status' => 'publish',
            'post_type'   => 'social_icons'
          ) ); 
          
          foreach( $posts as $post ){ ?>
            <a
              href="<?php the_field('link') ?>"
              class="footer-wrapper-social-link"
              title="Follow on Instagram"
              target="_blank"
              rel="nofollow"
            >
              <i class="<?php the_field('icon') ?>" aria-hidden="true"></i>
            </a>
         <?php }
         ?>
          
          </div>
        </div>
      </div>
      <?php } wp_reset_postdata(); // сброс ?>
    </footer>
    <?php wp_footer(); ?>
  

</html>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-1.4.1.min.js"></script>
    <script
      src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
      crossorigin="anonymous"
    ></script>
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
      crossorigin="anonymous"
    ></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/inc/Urich/ready-js/loupe.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/inc/Urich/ready-js/jquery.elevateZoom-3.0.8.min.js"></script>
    <!-- <script type="text/javascript" src="https://cdn.rawgit.com/igorlino/elevatezoom-plus/1.1.6/src/jquery.ez-plus.js"></script> -->
    <script src="<?php echo get_template_directory_uri() ?>/inc/Urich/ready-js/slick.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/inc/Urich/ready-js/scripts.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/inc/Urich/ready-js/common.js"></script>    
    <script src="<?php echo get_template_directory_uri() ?>/js/my.js"></script>
    