$(document).ready(function() {
  $(".design-thumb img")
    .on("mouseover", function() {
      $(this).css({ transform: "scale(1.5)" });
    })
    .on("mouseout", function() {
      $(this).css({ transform: "scale(1)" });
    })
    .on("mousemove", function(e) {
      $(this).css({
        "transform-origin":
          ((e.pageX - $(this).offset().left) / $(this).width()) * 100 +
          "% " +
          ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +
          "%"
      });
    });

  setTimeout(function() {
    var canvas0 = $(".design-thumb canvas")[0];
    canvas0.addEventListener("mousemove", function() {
      $(this)
        .on("mouseover", function() {
          $(this).css({ transform: "scale(1.5)" });
        })
        .on("mouseout", function() {
          $(this).css({ transform: "scale(1)" });
        })
        .on("mousemove", function(e) {
          $(this).css({
            "transform-origin":
              ((e.pageX - $(this).offset().left) / $(this).width()) * 100 +
              "% " +
              ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +
              "%"
          });
        });
    });
  }, 2000);
  setTimeout(function() {
    var canvas1 = $(".design-thumb canvas")[1];
    canvas1.addEventListener("mousemove", function() {
      $(this)
        .on("mouseover", function() {
          $(this).css({ transform: "scale(1.5)" });
        })
        .on("mouseout", function() {
          $(this).css({ transform: "scale(1)" });
        })
        .on("mousemove", function(e) {
          $(this).css({
            "transform-origin":
              ((e.pageX - $(this).offset().left) / $(this).width()) * 100 +
              "% " +
              ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +
              "%"
          });
        });
    });
  }, 2000);
  setTimeout(function() {
    var canvas2 = $(".design-thumb canvas")[2];
    canvas2.addEventListener("mousemove", function() {
      $(this)
        .on("mouseover", function() {
          $(this).css({ transform: "scale(1.5)" });
        })
        .on("mouseout", function() {
          $(this).css({ transform: "scale(1)" });
        })
        .on("mousemove", function(e) {
          $(this).css({
            "transform-origin":
              ((e.pageX - $(this).offset().left) / $(this).width()) * 100 +
              "% " +
              ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +
              "%"
          });
        });
    });
  }, 2000);
  setTimeout(function() {
    var canvas3 = $(".design-thumb canvas")[3];
    canvas3.addEventListener("mousemove", function() {
      $(this)
        .on("mouseover", function() {
          $(this).css({ transform: "scale(1.5)" });
        })
        .on("mouseout", function() {
          $(this).css({ transform: "scale(1)" });
        })
        .on("mousemove", function(e) {
          $(this).css({
            "transform-origin":
              ((e.pageX - $(this).offset().left) / $(this).width()) * 100 +
              "% " +
              ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +
              "%"
          });
        });
    });
  }, 2000);
});

// $(document).hover(function(event) {
//   console.log(event.target);
// });
// $(document).ready(function() {
//   $(".wp-post-image")
//     .first()
//     .addClass("zoom");
//   $(".zoom").loupe();
//   $(".loupe").css("width", "700px");
//   $(".loupe").css("height", "200px");

//   setTimeout(setCanvas, 500);
//   var dataURL = "";
// });

// function setCanvas() {
//   var canvas_block = document.querySelectorAll(".design-thumb");
//   var tabLength = canvas_block.length;
//   console.log(tabLength);
//   for (var i = 0; i < tabLength; i++) {
//     //  imgChange(canvas_block[i].childNodes);
//     console.log(canvas_block[i].childNodes);
//   }
// }

// function imgChange(imagePath) {
//   var c = document.getElementById("myCanvas");
//   var ctx = c.getContext("2d");
//   var img = new Image();
//   img.onload = function() {
//     ctx.drawImage(img, 0, 0);
//   };
//   img.src = imagePath;
// }

// if ($( document).width() > 1200) {
//   setTimeout(function() {
//     $(".design-thumb img").ezPlus({
//       zoomWindowFadeIn: 500,
//       zoomWindowFadeOut: 500,
//       lensFadeIn: 500,
//       lensFadeOut: 500
//     });
//   }, 4000);
// }

if ($(".no-results.not-found.error-wrapper").length != 0) {
  $("#main").css("padding", "0");
}
if ($(".no-results.not-found.error-wrapper").length != 0 && $(".wrapper-pagination").length != 0) {
  $("#primary").css("padding", "0");
  $("#main").css("padding", "0");
}

setTimeout(function() {
  $("#main").css("padding", "0");
}, 3000);

if ($("#empty-div-thankyou-page").length != 0) {
  $(".title-h2").css("display", "none");
  $("#primary").css("padding", "0");
  $(".entry-content")
    .css("max-width", "100vw")
    .css("position", "relative")
    .css("margin-left", "-50vw")
    .css("left", "50%");
}

$(document).ready(function() {
  $("#billing_first_name").attr("placeholder", "Enter your first name");
  $("#billing_last_name").attr("placeholder", "Enter your last name");
  $("#billing_city").attr("placeholder", "City");
  $("#billing_address_1").attr("placeholder", "Adress");
  $("#billing_postcode").attr("placeholder", "Index");
});

$(document).ready(function() {
  $(".button.e-custom-product").addClass("add-btn");
  $(".attachment-full.size-full.wp-post-image").css("width", 274);
  $(".attachment-152x167.size-152x167.wp-post-image").css("margin-top", "-39px");

  if ($(".a-tab-selected-item")) {
    $("#get_search_count").text($(".a-tab-selected-item").length);
  }
  if ($(".a-selected-item-line-wrapper").length != 0) {
    $("#get_search_count").text($(".a-selected-item-line-wrapper").length);
  }
});

function filterCategory(obj) {
  event.preventDefault();
  var id_cat = $(obj).attr("id_category");
  //console.log(id_cat);
  $.ajax({
    url: document.location.origin + "/wp-admin/admin-ajax.php",
    type: "post",
    data: {
      id_cat: id_cat,
      _wpnonce: document.location.origin + "/wp-admin/admin-ajax.php",
      action: "filter_product_shop_page"
    },
    success: function(data) {
      $(".plate.wrapper").html(data);
      // console.log(data);
    }
  });
}

function get_color(obj) {
  var color = $(obj).attr("id");
  // console.log(color);
  $("#color_input").val(color);
  // console.log($("#color_input").val());
}

$(document).ready(function() {
  $("#cart-tab").click(function() {
    $("#person-tab").removeClass("active");
    $("#bookmark-tab").removeClass("active");
    $("#cart-tab").addClass("active");

    $("#person").removeClass("show");
    $("#person").removeClass("active");
    $("#bookmark").removeClass("show");
    $("#bookmark").removeClass("active");
    $("#cart").addClass("show");
    $("#cart").addClass("active");
  });
  $("#bookmark-tab").click(function() {
    $("#person-tab").removeClass("active");
    $("#cart-tab").removeClass("active");
    $("#bookmark-tab").addClass("active");

    $("#person").removeClass("show");
    $("#person").removeClass("active");
    $("#cart").removeClass("show");
    $("#cart").removeClass("active");
    $("#bookmark").addClass("show");
    $("#bookmark").addClass("active");
  });
});

// $(document).ready(function() {
//   var page_account = document.location.origin + "/account/";
//   var click_cart = $("#shop_basket");
//   click_cart.click(function(e) {
//     e.preventDefault();
//     // console.log("click");
//     // console.log(page_account);
//     window.location.replace(page_account);
//     //  load_cart_tab();
//   });
// });

function load_cart_tab() {
  //console.log(obj);
  $.ajax({
    url: document.location.origin + "/wp-admin/admin-ajax.php",
    type: "post",
    data: {
      _wpnonce: document.location.origin + "/wp-admin/admin-ajax.php",
      action: "bild_cart_tab"
    },
    success: function(data) {
      // $(".tab-pane.fade.tab-pane-position").html(data);
      //  console.log(data);
    }
  });
}

function get_tab_cart() {
  //$("#person-tab").removeClass("active");
  $("#cart-tab").addClass("active");

  //$("#person").removeClass("show");
  // $("#person").removeClass("active");
  $("#cart").addClass("show");
  $("#cart").addClass("active");
}

$(document).ready(function() {
  var loginErrorDiv = document.getElementById("get_error_message_log_in");
  if (loginErrorDiv) {
    if (loginErrorDiv) {
      document.getElementById("get_error_text_log_in").style.display = "block";
    } else document.getElementById("get_error_text_log_in").style.display = "none";
  }
});

$(document).ready(function() {
  var get_page_regist = document.getElementById("password_1");
  if (get_page_regist) {
    document.getElementById("password_1").addEventListener("input", function() {
      get_true_pass();
    });
    document.getElementById("password_2").addEventListener("input", function() {
      get_true_pass();
    });
  }

  if (document.getElementById("show_massege_regist2")) {
    $("#show_massege_regist3").css("display", "block");
  }
});
function get_true_pass() {
  var pass3 = $("#password_1").val();
  var pass4 = $("#password_2").val();
  // console.log(pass3);
  if (pass4 != pass3) {
    $("#password_1").css("border", "solid 0.5px #e30613");
    $("#password_2").css("border", "solid 0.5px #e30613");
    $("#btn_registr_account").attr("disabled", "disabled");
  } else {
    $("#password_1").css("border", "solid 0.5px #efefef");
    $("#password_2").css("border", "solid 0.5px #efefef");
    $("#btn_registr_account").removeAttr("disabled");
  }
}

$(document).ready(function() {
  var page_my_account = document.location.origin + "/my-account/";
  var page_account = document.location.origin + "/account/";
  if (location.href == page_my_account) {
    $(".entry-content").addClass("login-wrapper");
    $(".woocommerce").addClass("wrapper");
    $(".woocommerce").addClass("login");
    if ($(".login-wrapper")) {
      $(".entry-title").css("display", "none");
    }
    if ($("div").is("#show_btn_lost_pass")) {
      $(".woocommerce-LostPassword.lost_password").css("display", "block");
    } else {
      $(".woocommerce-LostPassword.lost_password").css("display", "none");
    }
    $(".woocommerce-button.button.woocommerce-form-login__submit.add-btn").click(function(e) {
      //e.preventDefault();
      window.location.replace(page_account);
    });
  }
});

$(document).ready(function() {
  var title = $(".screen-reader-text");
  if (title) {
    title.css("display", "none");
  }
});

function custCreateCookie(name, value, days) {
  var expires;
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = "; expires=" + date.toGMTString();
  } else {
    expires = "";
  }
  document.cookie = name + "=" + value + expires + "; path=/";
}

function filterMode(obj, mode) {
  //console.log(mode);
  if ($(obj).hasClass("cust-no-active")) {
    // console.log(mode);
    $(".list-swap-wrapper").toggleClass("active");
    custCreateCookie("prod-type-view", mode, 30);
    window.location.reload(true);
  }
}

var alertSuccess = $(".woocommerce-notices-wrapper");

alertSuccess.css("display", "none");

$(document).ready(function() {
  var paramsString = location.search;
  var searchParams = new URLSearchParams(paramsString);
  //console.log(searchParams);
  //Iterate the search parameters.
  for (let p of searchParams) {
    console.log(p);

    // $.ajax({
    //   url: document.location.origin + "/wp-admin/admin-ajax.php",
    //   type: "post",
    //   data: {
    //     sort_name: p,
    //     _wpnonce: document.location.origin + "/wp-admin/admin-ajax.php",
    //     action: "bild_prod_view_sort"
    //   },
    //   success: function(data) {
    //   //  $(".wrapper.plate").html(data);
    //      console.log(data);
    //   }
    // });
  }
  //Сортировка select
  $("#sort").change(function() {
    var sortValues = $("#sort option:selected").val(); //получаем значение выбранного пункта select

    // console.log(p);

    if ($("#sort").val() == "") {
      searchParams.delete("select");
      window.history.replaceState({}, "", location.pathname + "?" + searchParams);
      location.reload();
    } else {
      searchParams.set("select", sortValues);
      window.history.replaceState({}, "", location.pathname + "?" + searchParams);
      location.reload();
    }
  });
});
$("#contact_form_btn_send").click(function(event) {
  event.preventDefault();

  var val_input = $(".contact-form-email").val();

  $.ajax({
    url: document.location.origin + "/wp-admin/admin-ajax.php",
    type: "post",
    data: {
      input_send_mess: val_input,
      _wpnonce: document.location.origin + "/wp-admin/admin-ajax.php",
      action: "send_message_at_home"
    },
    success: function(data) {
      //  console.log(data);
    }
  });
  var mess = $("#contact_form_btn_send");
  if (mess) {
    $(".message_popup_div").css("display", "block");
  }
  setTimeout(reload_page, 2000);
});

function reload_page() {
  location.reload();
}

$(".current-size-plus").click(function(e) {
  e.preventDefault();
  setTimeout(counterSum, 500);
});

$(".current-size-minus").click(function(e) {
  e.preventDefault();
  setTimeout(counterSum, 500);
});

$(document).ready(function() {
  var input_count = document.getElementsByClassName("current-size-quantity-2");
  for (var i = 0; i < input_count.length; i++) {
    input_count[i].addEventListener("input", function() {
      setTimeout(counterSum, 500);
    });
  }
});

$(document).ready(function() {
  if ($("#count_product_item").text() == "0") {
    // $(".single_add_to_cart_button.button.alt.add-btn").attr("disabled", "disabled");
    $(".button.alt.add-btn").attr("disabled", "disabled");
  }
});

function counterSum() {
  var arr = $(".current-size-quantity-2");
  var value_input_sum = 0;
  var symbol_price = $("#get_primalPrice").attr("get_symbol");
  var price_prod = parseFloat($(".get_price_by_product").attr("id"));
  var price_product = $("#get_price_product");
  for (var i = 0; i < arr.length; i++) {
    if (parseFloat($(arr[i]).val())) {
      value_input_sum += parseFloat($(arr[i]).val());
    }
  }
  //console.log(value_input_sum);
  // if (value_input_sum) {
  //   $(".single_add_to_cart_button.button.alt.add-btn").removeAttr("disabled");
  // } else {
  //   $(".single_add_to_cart_button.button.alt.add-btn").attr("disabled", "disabled");
  // }

  if (value_input_sum) {
    $("button.alt.add-btn").removeAttr("disabled");
  } else {
    $("button.alt.add-btn").attr("disabled", "disabled");
  }

  $("#count_product_item").text(value_input_sum);
  var inputAddCart = document.getElementsByClassName("input-text qty text");
  var price = getPrices(value_input_sum);

  inputAddCart[0].setAttribute("value", `${parseInt(value_input_sum)}`);

  price_product.text(price + symbol_price);
  $("#total_amount_price").text(value_input_sum * price + symbol_price);
  // plus_price(value_input_sum);
}
$(document).ready(function() {
  var from_count = document.querySelectorAll(".get_array_from_price");
  var prices = document.querySelectorAll(".get_array_prices");
  var prices_design = document.querySelectorAll(".get_array_prices_design");

  var tabLength = from_count.length;
  if (tabLength) {
    for (var i = 0; i < tabLength; i++) {
      $(".tierprice-table thead tr").first().append('<th class="print-price-kind-of-print">' + from_count[i].value + "</th>");
      $(".tierprice-table tbody tr").first().append(
        '<td class="printprices spezial_sieb_druck-1" style="display: table-cell;">' + prices[i].value + " $</td>"
      );


      $(".tierprice-table tbody tr").last().append(
        '<td class="printprices spezial_sieb_druck-1" style="display: table-cell;">' + prices_design[i].value + " $</td>"
      );
    }
  } else {
    $(".description-product-table").css("display", "none");
  }
});

function getPrices(value_input_sum) {
  var primalPrice = $("#get_primalPrice").val();
  var from_count = document.querySelectorAll(".get_array_from_price");
  var to_count = document.querySelectorAll(".get_array_to_price");
  var prices = document.querySelectorAll(".get_array_prices");
  var price_product = $("#get_price_product").text();
  var getPricesResult;
  var tabLength = from_count.length;

  for (var i = 0; i < tabLength; i++) {
    // console.log(from_count);
    // console.log(prices);
    if (from_count[i].value <= value_input_sum && value_input_sum <= to_count[i].value) {
      getPricesResult = prices[i].value;
    }
  }
  if (!getPricesResult) {
    getPricesResult = primalPrice;
  }

  return getPricesResult;
}

function plus_price(count_product) {
  var count_product = parseFloat(count_product);
  var price_product = $(".get_price_by_product").attr("id");
  total_price = count_product * price_product;
  $("#total_amount_price").text(total_price + "$");
}

function click_btn_add_cart(id_prod) {
  // console.log("submit");
  $(".form_add_size_to_cart").submit();
  // $.ajax({
  //   url: "http://demoproject.company/wp-admin/admin-ajax.php",
  //   type: "post",
  //   data: {
  //     id_prod: id_prod,
  //     value_input_s: value_input_s,
  //     value_input_m: value_input_m,
  //     value_input_l: value_input_l,
  //     value_input_xl: value_input_xl,
  //     value_input_xxl: value_input_xxl,
  //     value_input_3xl: value_input_3xl,
  //     _wpnonce: "http://demoproject.company/wp-admin/admin-ajax.php",
  //     action: "add_to_cart_prod"
  //   },
  //   success: function(data) {
  //     // $('#cust_ajax_block').html(data);
  //     console.log(data);
  //     //location.reload();
  //   }
  // });
}

function filter_sub_menu(obj) {
  //console.log(obj);
  var filter_name = $(obj).attr("id");
  $.ajax({
    url: document.location.origin + "/wp-admin/admin-ajax.php",
    type: "post",
    data: {
      filter_name: filter_name,
      _wpnonce: document.location.origin + "/wp-admin/admin-ajax.php",
      action: "bild_prod_view"
    },
    success: function(data) {
      $(".wrapper.plate").html(data);
      // console.log(data);
    }
  });
}

function click_to_cat(obj) {
  event.preventDefault();
  searchParams = $(obj).attr("name");
  $(".dgwt-wcas-search-input.header-search-input").val(searchParams);
  // console.log(obj);
  $(".dgwt-wcas-search-form").submit();
}

// $(".a-categorie.slick-slide.slick-current.slick-active").click(function(event) {
//   event.preventDefault();
//   searchParams = $(".a-categorie.slick-slide.slick-current.slick-active").attr(
//     "name"
//   );
//   console.log(searchParams);
//   $(".dgwt-wcas-search-input.header-search-input").val(searchParams);
//   // console.log(obj);
//   // $(".dgwt-wcas-search-form").submit();
// });

function clickPayment(obj) {
  console.log(obj);
}
